ARG BUILD_TAG=arm64

FROM forumi0721/test:${BUILD_TAG} as builder

ARG TARGETPLATFORM
ARG BUILDPLATFORM

LABEL maintainer="forumi0721@gmail.com"

RUN echo $TARGETPLATFORM >> /log
RUN echo $BUILDPLATFORM >> /log
